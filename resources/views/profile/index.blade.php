@extends('layouts.master')

@section("Judul2")
    <h1>USER PROFILE : {{ Auth::user()->name }}</h1>
@endsection

@section("content")

    <form action ="/profile/{{$profile->id}}" method="post">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label >Nama User</label>
            <input type="string" name ="name" value = "{{$profile->user->name}}" class="form-control" disabled>
        </div>
        
        <div class="form-group">
            <label >Email address</label>
            <input type="string" name ="email" value = "{{$profile->user->email}}" class="form-control" disabled>
        </div>
        <div class="form-group">
            <label >Umur</label>
            <input type="number" name ="umur" value = "{{$profile->umur}}" class="form-control">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>alamat</label>
            <input type="text" name = "alamat" value = "{{$profile->alamat}}" class="form-control">
        </div>
        @error('alamat')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Bio</label>
            <textarea name = "bio" class="form-control" cols="30" rows = "5">{{$profile->bio}}</textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection

