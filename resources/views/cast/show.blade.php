@extends('layouts.master')

@section("Judul")
    <h1>Biodata {{$cast -> nama}}</h1>
@endsection

@section("content")

    <h3> {{$cast -> nama}} </h3>
    <p> {{$cast -> umur}} </p>
    <p> {{$cast -> bio}} </p>

@endsection
