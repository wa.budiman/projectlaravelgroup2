@extends('layouts.master')

@section("Judul2")
    <h1>Bintang Film</h1>
@endsection

@section("content")
@auth
    <a href="/cast/create" class="btn btn-warning mb-3">Add data</a>
@endauth
<table class="table">
    <thead class="thead-dark">
        <tr>
          <th scope="col">#</th>
          <th scope="col">Nama</th>
          <th scope="col">Umur</th>
          <th scope="col">Bio</th>
          <th scope="col">Action</th>
         </tr>
      </thead>
    <tbody>
        @forelse ($cast as $key => $item)
        <tr>
            <td> {{$key +1 }}</td>
            <td> {{$item -> nama}}</td>
            <td> {{$item -> umur}}</td>
            <td> {{$item -> bio}}</td>
            <td>
                @auth
                <form action="/cast/{{$item->id}}" method = "POST">
                    @method('Delete')
                    @csrf
                    <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/cast/{{$item->id}}/edit" class="btn btn-primary btn-sm">Edit</a>
                    <input type ="submit" class="btn btn-danger btn-sm" value = 'Delete'> 
                </form>
                @endauth
                @guest
                    <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                @endguest

            </td>
        </tr>
        @empty
        <tr>
            <td> Data masih kosong</td>
        </tr>
        @endforelse
   </tbody>
</table>
@endsection