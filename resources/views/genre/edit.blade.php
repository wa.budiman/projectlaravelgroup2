@extends('layouts.master')

@section("Judul2")
    <h1>Edit GENRE Film {{$genre -> nama}}</h1>
@endsection

@section("content")
    <form action ="/genre/{{$genre->id}}" method="post">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label >Nama</label>
            <input type="string" name ="nama" value = "{{$genre->nama}}" class="form-control">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection