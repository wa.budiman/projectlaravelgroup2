<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\profile;

class profilecontroller extends Controller
{
    public function index(){
        $profile = profile::where('user_id', Auth::id())->first();
        return view('profile.index', compact('profile'));
    }
    public function update(Request $request, $id){
        $request->validate([
            'umur' => 'required',
            'bio' => 'required',
            'alamat'=>'required'
        ]);
        $profile = profile::find($id); 
            $profile -> umur = $request->umur;
            $profile -> alamat = $request->alamat;
            $profile -> bio = $request->bio;
        $profile->save();
        return redirect('/profile');
    }
}
