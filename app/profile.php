<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class profile extends Model
{
    protected $table = 'profile';
    protected $fillable = ['umur','bio','alamat','user_id'];
    
    public Function user() {
        return $this->belongsTo('App\User')->withDefault();
    }
}
