<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class film extends Model
{
    //
    protected $table = 'film';
    protected $fillable = ['judul','ringkasan','tahun','genre_id','poster'];

    public Function genre() {
        return $this->belongsTo('App\genre')->withDefault();
    }
    
}
