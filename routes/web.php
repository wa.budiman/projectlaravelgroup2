<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'filmController@index');
//Route::get('/register', 'AuthController@register');
//Route::post('/welcome', 'AuthController@signup');

Route::middleware('auth')->group(function () {
    route::resource('genre','genrecontroller');
});
// CRUD cast

Route::get('/cast/{cast_id}/edit', 'castcontroller@edit');
route::put('/cast/{cast_id}','castcontroller@update');
route::delete('/cast/{cast_id}','castcontroller@destroy');
Route::get('/cast/create', 'castcontroller@create');
Route::post('/cast', 'castcontroller@store');
Route::get('/cast', 'castcontroller@index');

Route::get('/cast/{cast_id}', 'castcontroller@show');

//CRUD film with ORM
route::resource('film','filmcontroller');
Auth::routes();
// profile
route::resource('profile','profilecontroller')->only(['index','update']);
Route::get('/home', 'HomeController@index')->name('home');
